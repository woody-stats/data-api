const {
  getData,
  sortPrincipalsByWeight,
  sortTitlesByYear,
} = require('../utils');
const {
  getTopRated,
  getTotalRuntime,
  getGenresStats,
  getEnrichedTitles,
} = require('./shared.js');

const getTitlesByDecade = titles => {
  const decades = {};

  titles.slice()
    .sort(sortTitlesByYear)
    .forEach(title => {
      const decade = Math.floor(title.year / 10) + '0';

      if (!decades[decade]) {
        decades[decade] = [];
      }

      decades[decade].push(title);
    })
  ;

  return Object.keys(decades)
    .map(decade => ({
      startYear: decade,
      titles: decades[decade],
    }))
  ;
};

const getDecadesStats = (req, res) => {
  const data = getData(req);
  const titlesByDecades = getTitlesByDecade(data.titles);

  const result = titlesByDecades.map(decade => {
    const enrichedTitles = getEnrichedTitles(decade.titles, data.names);
    const principalStats = {};

    enrichedTitles.forEach(title => {
      title.principals.forEach(principal => {
        if (!principalStats[principal.nameId]) {
          principalStats[principal.nameId] = {
            ...principal,
            weight: 0,
            titles: 0,
          };
        }

        principalStats[principal.nameId].weight += principal.weight;
        principalStats[principal.nameId].titles++;
      });
    });

    return {
      decade: decade.startYear,
      totalRuntime: getTotalRuntime(decade.titles),
      genres: getGenresStats(decade.titles),
      topRatedTitles: getTopRated(decade.titles
        .map(({ id, title, rating }) => ({ id, title, rating }))
      ),
      principalsStats: Object.values(principalStats).sort(sortPrincipalsByWeight),
      titles: enrichedTitles,
    };
  });

  res.json(result);
};

module.exports = { getDecadesStats };
