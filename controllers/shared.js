const {
  WOODY_NAME_ID,
  filterOnlyActors,
  filterNotWoody,
  enrichPrincipal,
  sortPrincipalsByWeight,
  sortTitlesByYear,
} = require('../utils');

module.exports = {
  getTopRated: (titles, count = 10) => titles
    .slice()
    .filter(title => title.rating)
    .sort((a, b) => b.rating.value - a.rating.value)
    .slice(0, count)
  ,
  getTotalRuntime: titles => titles.reduce((res, a) => res + a.runtime, 0),
  getGenresStats: titles => {
    const result = {};

    titles.forEach(title => {
      title.genres.forEach(genre => {
        if (!result[genre]) {
          result[genre] = 0;
        }

        result[genre]++;
      });
    });

    return Object.keys(result)
      .map(genre => ({
        name: genre,
        count: result[genre],
      }))
      .sort((a, b) => b.count - a.count)
    ;
  },
  getEnrichedTitles: (titles, names) => titles
    .map(title => ({
      ...title,
      woodyAge: title.year - names[WOODY_NAME_ID].birthYear,
      principals: title.principals
        .filter(filterOnlyActors)
        .filter(filterNotWoody)
        .filter(principal => names[principal.nameId])
        .map(enrichPrincipal(names))
        .map(principal => ({
          ...principal,
          ageInTitle: principal.birthYear
            ? title.year - principal.birthYear
            : null
          ,
        }))
        .sort(sortPrincipalsByWeight)
      ,
    }))
    .sort(sortTitlesByYear)
  ,
};
