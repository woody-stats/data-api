const { getData } = require('../utils');
const { getTopRated, getTotalRuntime, getGenresStats } = require('./shared.js');

const getSummaryStats = (req, res) => {
  const data = getData(req);

  var otherCount = 0;
  const genresStats = getGenresStats(data.titles).filter(genre => {
    if (genre.count < 4) {
      otherCount++;

      return false;
    }

    return true;
  });

  res.json({
    totalRuntime: getTotalRuntime(data.titles),
    genres: [
      ...genresStats,
      {
        name: 'other',
        count: otherCount,
      },
    ],
    titlesCount: data.titles.length,
    topRatedTitles: getTopRated(data.titles),
  });
};

module.exports = { getSummaryStats };
