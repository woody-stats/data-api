const { getData } = require('../utils');
const { getEnrichedTitles } = require('./shared.js');

const getTitles = (req, res) => {
  const data = getData(req);

  res.json(getEnrichedTitles(data.titles, data.names));
};

module.exports = {
  getTitles,
};
