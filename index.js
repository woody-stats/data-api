const express = require('express');
const readFile = require('./utils/read-file.js');
const routes = require('./routes.js');
const utils = require('./utils');

const port = process.env.PORT || 3333;
const app = express();

console.log('⭕️ | Loading data...')
readFile('data/dataset.json')
  .then(JSON.parse)
  .then(dataset => {
    console.log('🔴 | Data has been loaded');

    app.locals.data = dataset;
    app.locals.data.titles = dataset.movies
      .filter(utils.filterIsDirector)
      .filter(utils.filterOnlyDisplayingTypes)
      .sort(utils.sortTitlesByYear)
    ;
    delete app.locals.data.movies;

    app.use('/api', routes);

    app.listen(port, () => {
      console.log('⛳️ | Server started on port ', port);
    });
  })
  .catch(err => {
    console.log('⚠️  | Could not load data: ' + err);
  })
;
