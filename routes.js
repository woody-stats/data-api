const express = require('express');
const { getTitles } = require('./controllers/titles.js');
const { getDecadesStats } = require('./controllers/decades.js');
const { getSummaryStats } = require('./controllers/summary.js');
const router = express.Router();

router.get('/titles', getTitles);
router.get('/decades', getDecadesStats);
router.get('/summary', getSummaryStats);

module.exports = router;
