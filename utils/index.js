const WOODY_NAME_ID = 'nm0000095';

const displayingTypes = ['tvSeries', 'movie', 'tvMovie', 'short'];

module.exports = {
  getData: req => req.app.locals.data,

  enrichPrincipal: names => principal => ({
    ...principal,
    ...names[principal.nameId],
    sex: principal.type.includes('actor') ? 'male' : 'female',
  }),

  filterOnlyActors: principal => ['actor', 'actress'].includes(principal.type),
  filterNotWoody: principal => principal.nameId !== WOODY_NAME_ID,

  filterIsDirector: title => title.isDirector,
  filterOnlyDisplayingTypes: title => displayingTypes.includes(title.type),

  sortTitlesByYear: (a, b) => a.year - b.year,
  sortPrincipalsByWeight: (a, b) => b.weight - a.weight,

  WOODY_NAME_ID,
};
