const d3 = require('d3-dsv');
const readFile = require('./read-file.js');

const isUnique = (d, i, arr) => arr.indexOf(d) === i;

(async () => {
  const woodyId = 'nm0000095';
  var [movies, crews, names, ratings, principals] = await Promise.all([
    readFile('./data/title.basic.tsv').then(d3.tsvParse),
    readFile('./data/crew.tsv').then(d3.tsvParse),
    readFile('./data/name.tsv').then(d3.tsvParse),
    readFile('./data/title.ratings.tsv').then(d3.tsvParse),
    readFile('./data/title.principals.tsv').then(d3.tsvParse),
  ]);

  crews = crews.map(({tconst, directors, writers}) => ({
    movieId: tconst,
    directors: directors.split(','),
    writers: writers.split(','),
  }));
  ratings = ratings.map(({tconst, averageRating, numVotes}) => ({
    movieId: tconst,
    value: +averageRating,
    votes: +numVotes,
  }));
  principals = principals.map(({tconst, ordering, nconst, category}) => ({
    movieId: tconst,
    nameId: nconst,
    weight: 1 / +ordering,
    type: category,
  }));

  const checkIsDirector = crew => crew.directors.includes(woodyId);
  const checkIsWriter = crew => crew.writers.includes(woodyId);

  movies = movies.map(({tconst, titleType, primaryTitle, startYear, runtimeMinutes, genres}) => {
    const crew = crews.find(crew => crew.movieId === tconst) || null;
    const rating = ratings.find(rating => rating.movieId === tconst) || null;
    const principalsList = principals
      .filter(p => p.movieId === tconst)
      .map(({ nameId, weight, type }) => ({
        nameId,
        weight,
        type,
      }))
    ;

    return {
      id: tconst,
      type: titleType,
      title: primaryTitle,
      year: +startYear,
      runtime: +runtimeMinutes,
      genres: genres.toLowerCase().split(','),

      isWritter: checkIsWriter(crew),
      isDirector: checkIsDirector(crew),

      crew: crew ? ({
        directors: crew.directors,
        writers: crew.writers,
      }) : null,

      principals: principalsList,

      rating: rating ? ({
        value: rating.value,
        votes: rating.votes,
      }) : null,
    };
  });

  names = names.reduce((res, {nconst, primaryName, birthYear, deathYear, primaryProfession}) => ({
    ...res,
    [nconst]: {
      name: primaryName,
      birthYear: +birthYear,
      deathYear: deathYear !== '\N' ? +deathYear : null,
      profession: primaryProfession.toLowerCase().split(','),
    },
  }), {});

  const professions = Object.values(names)
    .reduce((res, { profession }) => ([...res, ...profession]), [])
    .filter(isUnique)
  ;
  const principalTypes = principals
    .map(({ type }) => type)
    .filter(isUnique)
  ;
  const genres = movies
    .reduce((res, { genres }) => ([...res, ...genres]), [])
    .filter(isUnique)
  ;
  const movieTypes = movies
    .map(({ type }) => type)
    .filter(isUnique)
  ;

  console.log(JSON.stringify({
    movies,

    names,

    professions,
    principalTypes,
    genres,
    movieTypes,
  }));
})();
