const fs = require('fs');

module.exports = path => new Promise((resolve, reject) => {
  fs.readFile(path, 'utf-8', (err, data) => {
    if (!err) {
      resolve(data);
    } else {
      reject(err);
    }
  });
});
